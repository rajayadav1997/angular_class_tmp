import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingStuffComponent } from './binding-stuff.component';

describe('BindingStuffComponent', () => {
  let component: BindingStuffComponent;
  let fixture: ComponentFixture<BindingStuffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingStuffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingStuffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit ,Input,ContentChild} from '@angular/core';

@Component({
  selector: 'app-binding-stuff',
  templateUrl: './binding-stuff.component.html',
  styleUrls: ['./binding-stuff.component.css']
})
export class BindingStuffComponent implements OnInit {

  // @Input() name: string;

  @ContentChild("parentRef",null) bbb;

  constructor() { }

  ngOnInit() {
    console.log(this.bbb)
  }

}

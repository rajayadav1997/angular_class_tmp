import { Injectable, EventEmitter } from '@angular/core';
import { FService } from './f.service';

@Injectable({
  providedIn: 'root'
})
export class AbcService {


  statusUpdate = new EventEmitter<string>();
  
  constructor(private sService: FService) { }


   public static i = 0;

  af(){
    console.log("service obj found ",this.statusUpdate);
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, NgForm, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Hero } from './Hero';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
@Component({
  selector: 'app-driven-form',
  templateUrl: './driven-form.component.html',
  styleUrls: ['./driven-form.component.css']
})
export class DrivenFormComponent implements OnInit {
  model: any = {};


  val="sdasas";

  

// VIEW CHILd
  @ViewChild('frm',null) public userFrm: NgForm;
  public showFrm(): void{
    console.log(this.userFrm.form.value);
  }
//********** */


// ****************
userlogin = new FormData;
onClickSubmit(myvalue){
  // this.userlogin.Reset();
    console.log('form value ',myvalue);

    // let model = new FormGroup({
    //   "foo": new FormControl()
    // });
    // model.reset();
  }
  // *************
  time:any;
constructor(private formBuilder: FormBuilder, private http: HttpClient){
//   this.formdata = new FormGroup({
//     emailid: new FormControl("angular@gmail.com"),
//     passwd: new FormControl("abcd1234")
//  });


this.time = new Observable<string>((observer: Observer<string>) => {
  setInterval(() => observer.next(new Date().toString()), 1000);
});

}

  
// search = new FormControl('hello');
registerForm: FormGroup;
submitted = false;
ngOnInit(): void {
  this.registerForm = this.formBuilder.group({
    firstName: ['abc', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, this.ageRangeValidator],this.asynccall]
  });

  //  with the help of subscribe we can get value of input field.
  this.registerForm.get('firstName').valueChanges.subscribe(
    uname => {
    console.log('Username changed:' + uname);
    this.registerForm.setValue({ 'firstName': 'sds' });
    }
  );
}

asynccall(){
  console.log('async call');
}
ageRangeValidator(control: AbstractControl): { [key: string]: boolean } | null {
console.log('my value ',control.value );


  if (control.value !== undefined && (isNaN(control.value) || control.value < 18 || control.value > 45)) {

      return { 'ageRange': true };

  }

  return null;

}

get f() { return this.registerForm.controls; }

asyccall(){
  let httpOptions = {
    headers: new HttpHeaders({
    'Content-type': 'application/json'
})};
 this.http.get("https://reqres.in/api/users?page=2",httpOptions).subscribe((res) =>{
  console.log(res);
}, error => {
  console.log(error);
  // this.spinnerProgress = false;
});

}

onreactvalSubmit(){

this.asyccall()

  this.submitted=true;
  if (this.registerForm.invalid) {
    return;
}else{
console.log('submit');
}
}



  onSubmit(){
    
  }


}

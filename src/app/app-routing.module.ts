import { NgModule } from '@angular/core';
import { Routes, RouterModule,Params } from '@angular/router';

import { AbcComponent } from './abc/abc.component';
import { ChildComponent } from './abc/child/child.component';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth.guard';
import {CanActivate , CanDeactivate} from "@angular/router";
import { HnResolver } from './mainclass';
import { DrivenFormComponent } from '../app/driven-form/driven-form.component';

const routes: Routes = [
  {  
    path:"abc/:id",
    component:AbcComponent,
    resolve: {message: HnResolver} 
  },
  { 
    path:"form",
    component:DrivenFormComponent
  }
  
  // { path: 'new', component: ChildComponent,outlet:'aux'} 

];
// imports: [RouterModule.forRoot(routes, { useHash: true }),
@NgModule({
  imports: [RouterModule.forRoot(routes),
    LoggerModule.forRoot({ level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR}),
    HttpClientModule ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AbcComponent } from './abc/abc.component';
import {CanActivate,CanDeactivate} from "@angular/router";
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanDeactivate<AbcComponent>{
  canDeactivate(component: AbcComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
 
     return window.confirm("Are you sure?");
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return true;
  }
  
}

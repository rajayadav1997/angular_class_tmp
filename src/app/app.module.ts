import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChildComponent } from './abc/child/child.component';
import { AbcComponent } from './abc/abc.component';
import { BindingStuffComponent } from './binding-stuff/binding-stuff.component';
import { AbcdirDirective } from './abcdir.directive';
import { AbcService } from './abc.service';
import { EmojiModule } from '@ctrl/ngx-emoji-mart/ngx-emoji';
import { AbcpipisPipe } from './abcpipis.pipe';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { DrivenFormComponent } from './driven-form/driven-form.component';
import {HttpClientModule} from '@angular/common/http';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import {EmojiFrequentlyService,EmojiSearch} from '@ctrl/ngx-emoji-mart';
import { FirebasedemoComponent } from './firebasedemo/firebasedemo.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../app/firebasedemo/firebasedemo.component';

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    AbcComponent,
    BindingStuffComponent,
    AbcdirDirective,
    AbcpipisPipe,
    DrivenFormComponent,
    FirebasedemoComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    PickerModule,
    EmojiModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
 	AngularFirestoreModule,
  ],
  providers: [EmojiFrequentlyService,EmojiSearch],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAnTdTYG5Z_odb9yfpAkLUHw0GOV943FJ8',
    authDomain: 'boreal-ellipse-253307.firebaseapp.com',
    databaseURL: 'https://boreal-ellipse-253307.firebaseio.com',
    projectId: 'boreal-ellipse-253307',
    storageBucket: 'boreal-ellipse-253307.appspot.com',
    messagingSenderId: '1004578154527'
  }
};

@Component({
  selector: 'app-firebasedemo',
  templateUrl: './firebasedemo.component.html',
  styleUrls: ['./firebasedemo.component.css']
})

// export const environment = {
//   production: false,
//   firebase: {
//     apiKey: 'AIzaSyAnTdTYG5Z_odb9yfpAkLUHw0GOV943FJ8',
//     authDomain: 'boreal-ellipse-253307.firebaseapp.com',
//     databaseURL: 'https://demodb.firebaseio.com',
//     projectId: 'boreal-ellipse-253307',
//     storageBucket: "",
//     messagingSenderId: ''
//   }
// };


export class FirebasedemoComponent implements OnInit {

  constructor(private db: AngularFirestore) { }

  data : any;
  ngOnInit() {
      // this.db.collection('demo').add({
      //   name: 'value.name',
      //   id: '5'
      // }).then(res => {}, err => (err));
  
      this.db.collection('demo').snapshotChanges()
      .subscribe(snapshots => {
        this.data = snapshots;
        console.log(snapshots);
      });
    
    }
  
  
}
